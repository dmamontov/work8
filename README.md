Первый случай работает в любом состоянии. Вторые два только с выключенным selinux.

1) 
Нужно привести конфигурацию заугрузки близкую к resque mode. Иначе загрузиться не получится.
Конфигурация граба:


https://yadi.sk/i/CzXU95-snW4Whg

загружаем поллитики селинукс. меняем пароль. продолжаем загрузку выполнив /sbin/init

https://yadi.sk/i/eJL7c_W-Z9paTQ

2)
Аналогично. Удаляем всё лишнее из linux16 и пишем rd.break

https://yadi.sk/i/VU62bO4cymonCA

Чрутим, меняем пароль, ребутимся

https://yadi.sk/i/2CYjO9yr9vo4lw

3)
Аналогично с предыдущим примером

https://yadi.sk/i/u1jKZ6kR3L47qQ

https://yadi.sk/i/KCb-lLH8h-P5Qw